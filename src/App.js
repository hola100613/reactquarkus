import logo from './logo.svg';
import './App.css';

import { BrowserRouter, Route, Routes } from 'react-router-dom';

import { useEffect, useState } from 'react';
import Keycloak from 'keycloak-js';

import CompShowModulo from './Modulo/ShowModulo';

const keycloakOptions = {
  url: "https://keycloak-proyecto-workflow-invoke.apps.rosa-79p7v.nyhz.p1.openshiftapps.com/auth/",
  realm: "login-realm",
  clientId: "login-client",
}


function App() {

  const [keycloak, setKeycloak] = useState(null)

  useEffect( ()=> {
    const initKeycloak = async () => {
      const keycloakInstance = new Keycloak(keycloakOptions)
      try{
        await keycloakInstance.init({onLoad: 'login-required'})
        setKeycloak(keycloakInstance)
        if(keycloakInstance.authenticated){
          console.log(keycloakInstance);
        }
      }catch (error){
        console.log(`Error ${error}`);
      }
    }
    initKeycloak()
  },[])

  const handleLogout = () => {
    if(keycloak){
      keycloak.logout()
    }
  }
  
  return (
    <div className="App">
          <div>
            <header className="App-header">
              <img src={logo} className="App-logo" alt="logo" />
              <p>
                Edit <code>src/App.js</code> and save to reload.
              </p>
              <a
                className="App-link"
                href="https://reactjs.org"
                target="_blank"
                rel="noopener noreferrer"
              >
                Learn React
              </a>
            </header>
            <div>
              <h1>HOLA MUNDO</h1>
              <BrowserRouter>
                <Routes>
                  <Route path='/' element={<CompShowModulo />} />
                </Routes>
              </BrowserRouter>
              <button onClick={handleLogout}>salir</button>
            </div>
          </div>
    </div>
  );
}

export default App;
