import { useEffect, useState } from 'react';

const  CompShowModulo = ()=> {

    const [modulo, setModulo] = useState([]);

    useEffect(() => {
      const token = 'eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICItV3IwNHpDb2Q3Xy1DY1hZQlJ4Slp1Q1FaOV9NUklwNzNkVXljM04zNmFrIn0.eyJleHAiOjE3MTU5OTQ4NjksImlhdCI6MTcxNTk1ODg2OSwianRpIjoiZDA3MzdiMDctMzFiNS00OTUwLWFiNmMtNGM0MDQzMmI1MzliIiwiaXNzIjoiaHR0cHM6Ly9rZXljbG9hay1wcm95ZWN0by13b3JrZmxvdy1pbnZva2UuYXBwcy5yb3NhLTc5cDd2Lm55aHoucDEub3BlbnNoaWZ0YXBwcy5jb20vYXV0aC9yZWFsbXMvcmVhbG0tYXBpIiwiYXVkIjoiYWNjb3VudCIsInN1YiI6IjFiM2YxZjY4LWM5YzgtNGY0Ny1iMzc5LWI2OGUxYmU2YTdkYSIsInR5cCI6IkJlYXJlciIsImF6cCI6ImNsaWVudC1hcGkiLCJzZXNzaW9uX3N0YXRlIjoiOTJmOWRmYzUtMWJjOC00ZDhjLThkNjgtMGViYmY0Y2NhMTVhIiwiYWNyIjoiMSIsImFsbG93ZWQtb3JpZ2lucyI6WyIvIl0sInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIiwiZGVmYXVsdC1yb2xlcy1yZWFsbS1hcGkiXX0sInJlc291cmNlX2FjY2VzcyI6eyJhY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50IiwibWFuYWdlLWFjY291bnQtbGlua3MiLCJ2aWV3LXByb2ZpbGUiXX19LCJzY29wZSI6ImVtYWlsIHByb2ZpbGUiLCJzaWQiOiI5MmY5ZGZjNS0xYmM4LTRkOGMtOGQ2OC0wZWJiZjRjY2ExNWEiLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsIm5hbWUiOiJhZG1pbiBhZG1pbiIsInByZWZlcnJlZF91c2VybmFtZSI6ImFkbWluIiwiZ2l2ZW5fbmFtZSI6ImFkbWluIiwiZmFtaWx5X25hbWUiOiJhZG1pbiIsImVtYWlsIjoiYWRtaW5AZ21haWwuY29tIn0.HsgnjYUsWbrIK5kShztX9qK3iEBbPcfdr2k4uFKnVDO1F7wUPSY6Lhot9sDA6MPLwndIFet9GRPL262aNdJ7YJH8A_JzDAfeKGmtdsLg-gUpbXGLxpgVXvBwkbasKWdmC1vMhX5rbmfDPURaM1jWcBD8Kcjt5h1d0wvs9xhHhcu0njoCrbK-IFIwKwGn6CPhYzYueUtYuONF3pLCJCJgp135SRY9EpAZVFcsPj3WP0b8ncGg_vzqKMi3SDfAdZufgUPJLKClM6SpdzgtpoPLL5I9qvln6Ri1xppKiAXbJ5H-3q8HNAn06aLtN0h13cJPiC78lFPMvgB2EPdEEqovBQ';
  
      fetch('https://api-v-1-1-git-proyecto-workflow-invoke.apps.rosa-79p7v.nyhz.p1.openshiftapps.com/Modulo', {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`,
        },
      })
      .then(response => response.json())
      .then(data => {
        setModulo(data);
      })
      .catch(error => {
        console.error('Error al hacer la solicitud:', error);
      });
    }, []);
  
    return (
      <div>
        <h1>Módulos</h1>
        <ul>
          {modulo.map(modulo => (
            <li key={modulo.idModulo}>
              <strong>ID:</strong> {modulo.idModulo}, 
              <strong> Nombre:</strong> {modulo.nombreModulo}, 
              <strong> Operación:</strong> {modulo.operacionModulo}
            </li>
          ))}
        </ul>
      </div>
    );
  }

export default CompShowModulo